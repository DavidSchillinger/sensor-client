/*global angular  */

/* we 'inject' the ngRoute module into our app. This makes the routing functionality to be available to our app. */
var myApp = angular.module('myApp', ['ngRoute'])

/* the config function takes an array. */
myApp.config(['$routeProvider', ($routeProvider) => {
	$routeProvider
	.when('/search', {
		templateUrl: 'templates/search.html',
		controller: 'searchController'
	})
	.when('/readings', {
		templateUrl: 'templates/readings.html',
		controller: 'readingsController'
	})
	.when('/register', {
		templateUrl: 'templates/register.html',
		controller: 'registerController'
	})
	.otherwise({
		redirectTo: 'search'
	})
}])

myApp.controller('searchController', ($scope, $http) => {
	$scope.message1 = 'Sensor owner:'
	$scope.message2 = 'Sensor name:'
	$scope.message3 = 'Sensor type:'
	$scope.method = 'GET'
	  
	$scope.search = ($event) => {
		console.log('search()')
		if ($event.which == 13) {
			$scope.error1 = ''
			$scope.error2 = ''
			if ($scope.owner === undefined) {
				$scope.error1 = ' Please enter an owner.'
			} else {
				var headers = {'Authorization': 'Basic ZGF2aWRzOnNjaGlsbGlk', 'Accept': 'application/json', 'owner': $scope.owner}
				if ($scope.sensorName !== undefined && $scope.sensorName !== null && $scope.sensorName !== '') {
					var url = 'https://sensor-data-api-davidsch.c9users.io/sensors?name=' + $scope.sensorName
					$http({method: $scope.method, url: url, headers: headers})
					.then((response) => {
						var buildObject = response.data.sensor
						buildObject.id = response.data.id
						var sensorArray0 = Array()
						sensorArray0.push(buildObject)
						$scope.sensors = sensorArray0
						$scope.amount = 1
					}), 
					(error) => {
						$scope.error2 = error
					}
				} else if ($scope.sensorName !== undefined && $scope.sensorName !== null && $scope.sensorName !== '') {
					var url = 'https://sensor-data-api-davidsch.c9users.io/sensors?type=' + $scope.sensorType
					$http({method: $scope.method, url: url, headers: headers})
					.then((response) => {
						$scope.sensors = response.data.sensors
						$scope.amount = response.data.sensors.length
					}), 
					(error) => {
						$scope.error2 = error
					}
				} else {
					var url = 'https://sensor-data-api-davidsch.c9users.io/sensors'
					$http({method: $scope.method, url: url, headers: headers})
					.then((response) => {
						$scope.sensors = response.data.sensors
						$scope.amount = response.data.sensors.length
					}), 
					(error) => {
						$scope.error2 = error
					}
				}
			}
		}
	}
})

myApp.controller('readingsController', ($scope, $http) => {
	$scope.method = 'GET'
	$scope.message = 'This is a visualization of sensor data from the sensor data API. The sensor values here are updated periodically by sensors that send the values to my API.'
	$scope.message1 = 'Note: The sensors are not really precise so values might seem unrealistic.'
	$scope.temperature = 'Temperature reading in Celsius:'
	$scope.temperature1 = '00'
	$scope.temperature2 = '.00'
	$scope.light = 'LDR reading in Lux:'
	$scope.lux = '0000'
	$scope.pot = 'Potentiometer reading, 0-1024:'
	$scope.potRead = 0000
	$scope.switch = 'Switch reading, 0 or 1:'
	$scope.switchRead = 0
	
	$scope.reload = ($event) => {
		var headers = {'Authorization': 'Basic ZGF2aWRzOnNjaGlsbGlk', 'Accept': 'application/json', 'owner': 'RaspberryPi'}
		var url = 'https://sensor-data-api-davidsch.c9users.io/sensors'
		$http({method: $scope.method, url: url, headers: headers})
		.then((response) => {
			$scope.lux = response.data.sensors[2].reading
			$scope.potRead = response.data.sensors[3].reading
			$scope.switchRead = response.data.sensors[1].reading
			var temp = response.data.sensors[0].reading
			temp = temp.split('.')
			$scope.temperature1 = temp[0]
			$scope.temperature2 = '.'+temp[1]
		}).catch((error) => {
			$scope.errorMSG = error.data
		})
	}
})

myApp.controller('registerController', ($scope, $http) => {
	$scope.message = 'Register a new sensor here before updating it.'
	$scope.method = 'POST'
	$scope.register = ($event) => {
		
		$scope.message1 = $scope.returnedName = $scope.returnedType = $scope.returnedID = $scope.message2 = $scope.errorMSG = ''
		
		if ($scope.name === undefined || $scope.name === null || $scope.name === '') {
			$scope.nameError = ' Please enter a name for your new sensor.'
		} else if ($scope.type === undefined || $scope.type === null || $scope.type === '') {
			$scope.typeError = ' Please enter a type for your new sensor.'
		} else {
			var headers = {'Authorization': 'Basic ZGF2aWRzOnNjaGlsbGlk', 'Accept': 'application/json'}
			var url = 'https://sensor-data-api-davidsch.c9users.io/sensors'
			var data = {owner: 'RaspberryPi', name: $scope.name, reading: 'Initialized sensor.', type: $scope.type}
			
			$http({method: $scope.method, url: url, headers: headers, data: data})
			.then((response) => {
				$scope.message1 = 'The new sensor was registered with the following data:'
				$scope.returnedName = 'Name: '+response.data.data.sensor.name
				$scope.returnedType = 'Type: '+response.data.data.sensor.type
				$scope.returnedID = 'ID: '+response.data.data.sensor.id
				$scope.message2 = 'This ID is required to update your new sensor.'
			}).catch((error) => {
				$scope.errorMSG = error.data.message
			})
		}
	}
})
